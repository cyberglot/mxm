#mxm

```bash
$ stack build
$ stack install
$ sh fetch-data.sh
$ mxm mxm_dataset_train.txt
```
## Credit

Initial version (parsing and main) was provided by Ken Larsen. Since then, the project has changed and initial version was partially rewritten (if not completely), turned into a stack project and more code added.
