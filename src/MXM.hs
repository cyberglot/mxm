{-# LANGUAGE OverloadedStrings #-}

module MXM where
import Data.Text (Text)
import Data.Attoparsec.Text
import qualified Data.Attoparsec.Internal.Types as AttT
import qualified Data.Text as T
import Data.List.Split (chunksOf)
import qualified Data.Map as M
import Data.List (sortBy)
import Data.Either (isRight)
import Control.Parallel.Strategies (parMap, rpar)

readMXM :: T.Text
        -> Either String ([T.Text], [(T.Text, Int, [(Int, Int)])])
readMXM = parseOnly (bagOfWords <* endOfInput)

bagOfWords :: AttT.Parser T.Text
              ([T.Text], [(T.Text, Int, [(Int, Int)])])
bagOfWords = do
  skipMany commentline
  _words <- wordsline <* endOfLine
  tracks <- many1 (trackline <* endOfLine)
  return (_words, tracks)

readWordsline :: Text -> Either String [Text]
readWordsline = parseOnly (wordsline <* endOfInput)

wordsline :: Parser [Text]
wordsline = char '%' *> (word `sepBy1` char ',')
  where word = takeWhile1 (\c -> c /= ',' && c /= '\n' && c /= '\r')

commentline :: AttT.Parser T.Text ()
commentline = char '#' *> skipWhile notEndOfLine *> endOfLine
  where notEndOfLine c = c /= '\n' && c /= '\r'

readTrackline :: Text
              -> Either String (Text, Int, [(Int, Int)])
readTrackline = parseOnly (trackline <* endOfInput)

trackline :: Parser (Text, Int, [(Int, Int)])
trackline = do
  track <- takeTill (== ',')
  _ <- char ','
  mid <- decimal
  _ <- char ','
  wc <- wordcounts
  return (track, mid, wc)

wordcounts :: Parser [(Int, Int)]
wordcounts = wordcount `sepBy'` char ','

wordcount :: Parser (Int, Int)
wordcount = (,) <$> decimal <* char ':' <*> decimal

parseWordsAndTracks :: [Text]
                    -> (Either String [Text],
                        [Either String (Text, Int, [(Int, Int)])])
parseWordsAndTracks [] =
  (Left "No words line found\n", [Left "No tracks found\n"])
parseWordsAndTracks (w : ts) =
  (readWordsline w, map readTrackline ts)

removeComments :: Text -> [Text]
removeComments file = filter (not . T.isPrefixOf "#") (T.lines file)

validTracks :: [Either String (Text, Int, [(Int, Int)])]
            -> [(Text, Int, [(Int, Int)])]
validTracks = map validTrack . filter isRight

validTrack :: Either String (Text, Int, [(Int, Int)])
           -> (Text, Int, [(Int, Int)])
validTrack (Left _)  = (T.empty, 0, [(0, 0)])
validTrack (Right t) = t

showTopWords :: [(Text, Int)] -> String
showTopWords [] = ""
showTopWords ws = mappend "Top10:\n" $ indexIt ws >>= ranking

ranking :: ((Text, Int), Int) -> String
ranking ((word, occurrences), idx) =
  T.unpack word ++ "(" ++ show (idx + 1) ++ "): " ++ show occurrences ++ "\n"

indexIt :: [a] -> [(a, Int)]
indexIt xs = zipWith (,) xs [0..]

-- list is chunked to be processed in parallel
top10Words :: [Text] -> [(Text, Int, [(Int, Int)])] -> [(Text, Int)]
top10Words ws =
  wordLookUp ws . fst10 . sortByFreq . wordFreq . chunksOf 150 . map idAndCount

wordLookUp :: [Text] -> [(Int, Int)] -> [(Text, Int)]
wordLookUp ws = map (\(a, b) -> (ws !! (a - 1), b))

idAndCount :: (Text, Int, [(Int, Int)]) -> [(Int, Int)]
idAndCount (_, _, ids) = ids

fst10 :: [a] -> [a]
fst10 [] = []
fst10 xs = zipWith const xs [1..10]

wordFreq :: [[[(Int, Int)]]] -> [(Int, Int)]
wordFreq = mergeFreqs . parMap rpar mergeFreqs

-- M.fromListWith => O(n * log n)
-- M.toList       => O(n)
mergeFreqs :: [[(Int, Int)]] -> [(Int, Int)]
mergeFreqs = M.toList . M.fromListWith (+) . concat

sortByFreq :: [(Int, Int)] -> [(Int, Int)]
sortByFreq = reverse . sortBy byFreq
  where
    byFreq (_, fq1) (_, fq2) = compare fq1 fq2
