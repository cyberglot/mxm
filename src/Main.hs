{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Data.Text.IO as T
import Data.Either (isRight)
import System.Environment
import MXM
import Text.Printf


main :: IO ()
main = do
  fs <- getArgs

  case fs of

    [] -> do
      putStrLn "Error: No file specified"
      return ()

    (f:_) -> do
      file <- T.readFile f

      let (wordsRes, tracks) = parseWordsAndTracks . removeComments $ file

      case wordsRes of
        Right _words -> do
          printf "File size: %d chars. Words: %d. Tracks: %d\n"
            (T.length file) (length _words) (length $ filter isRight tracks)
          printf . showTopWords . top10Words _words . validTracks $ tracks
        Left _ -> do
          printf "No words line found\n"
